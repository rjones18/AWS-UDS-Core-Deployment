aws_owner_id        = "099720109477"
aws_ami_name        = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
vpc_name            = "reg-project-vpc"
ec2_type            = "c5.2xlarge"
public_subnet_name  = "web-subnet-1"
number_of_instances = 1